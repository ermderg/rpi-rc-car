# README #


###RC Car###

* This is a RC Car that runs on a Raspberry PI and Motor Controller with some other hardware as well as a Bluetooth Controller.
* 0.0.1

### How do I get set up? ###

* You will need this code and some hardware to make  the same car.
* You will need: 1 X 2 Wheel Drive Car Kit, 1 x LN298H Motor Controller, 5 X Jumper cables, 1 X USB Battery Bank, 1 X Raspberry Pi (3 is best), 1 X Bluetooth Controller (I used a PS3 one)
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* The RC Car is being controlled using a PS3 controller and the built-in Bluetooth on the Raspberry PI 3 or Bluetooth dongle
* The Code has been testing and no bugs have been found if you find any please contact me via the email address
* Please be aware this project can take a will to make and be prepared to put a lot of time debugging the code and any hardware issues 

### Future Plans ###

* Future plans for the car are better code and a more effective and accurate control system (Mainly the controller)

### Who do I talk to? ###

* Thomas Tisbury - t.tisbury2000@gmail.com
* Other community or team contact