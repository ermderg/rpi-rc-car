#!/usr/bin/env python

import os, sys, pygame 
from pygame import locals
import Robot
import time
from espeak import espeak
CrazyMode = "No"
CarRunning = "Yes"

os.environ["SDL_VIDEODRIVER"] = "dummy"
pygame.init()

pygame.joystick.init() # main joystick device system

deadZone = 0.6 # make a wide deadzone
try:
   j = pygame.joystick.Joystick(0) # create a joystick instance
   j.init() # init instance
   print ('Enabled joystick: ' + j.get_name())
except pygame.error:
   print ('no joystick found.')


while CarRunning == "Yes":
   for e in pygame.event.get(): 
       if e.type == pygame.locals.JOYAXISMOTION: # Read Analog Joystick Axis
         x1 , y1 = j.get_axis(0), j.get_axis(1) # Left Stick
         y2 , x2 = j.get_axis(2), j.get_axis(3) # Right Stick

         if CrazyMode == "No":  
          if y1 < -1 * deadZone:
             print("Forward")
             Robot.forward(0.1)
          
                    
          if y1 > 1 * deadZone:
             print("Reverse")
             Robot.reverse(0.1)
          

          if x1 < -1 *deadZone:
              print("Left")
              Robot.left(0.1)
          

          if x1 > 1 * deadZone:
              print("Right")
              Robot.right(0.1)
          

          if x2 < -1 * deadZone:
             print("Crazy Mode Activated")
             CrazyMode = "Yes"
             time.sleep(0.5)
         break

         if CrazyMode == "Yes": 
          if y1 < -1 * deadZone:
             print("Reverse")
             Robot.reverse(0.1)
          
                    
          if y1 > 1 * deadZone:
             print("Forward")
             Robot.forward(0.1)
          

          if x1 < -1 *deadZone:
              print("Right")
              Robot.right(0.1)
          

          if x1 > 1 * deadZone:
              print("Left")
              Robot.left(0.1)
          

          if x2 < -1 * deadZone:
             print("Crazy Mode Deactivated")
             CrazyMode = "No"
             time.sleep(0.5)
         break
